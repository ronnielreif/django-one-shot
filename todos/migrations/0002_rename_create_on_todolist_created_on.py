# Generated by Django 5.0 on 2023-12-12 20:06

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todolist",
            old_name="create_on",
            new_name="created_on",
        ),
    ]
